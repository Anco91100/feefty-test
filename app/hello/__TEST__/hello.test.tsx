import { render, screen } from '@testing-library/react';
import Home from '../page';
import React from 'react';

describe('Home', () => {
  test('renders "HELLO" text', () => {
    render(<Home />);
    const helloText = screen.getByText('HELLO');
    expect(helloText).toBeInTheDocument();
  });
});