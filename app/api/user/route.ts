import { UserService } from '@/lib/userService';
import { PrismaClient } from '@prisma/client';
import { NextResponse } from 'next/server';

const prisma = new PrismaClient()
const service = UserService;

interface IUser {
    firstName: string,
    lastName: string,
    role: string,
    email: string
}

interface ICreateUserRequest {
    firstName: string,
    lastName: string,
    role: string,
    email: string
}

interface IUpdateUserRequest extends IUser {
    id: string
}

interface IDeleteUserRequest {
    id: string;
}

export async function GET(request: Request) {
    const users = await service.getAllUsers();
    return NextResponse.json(users);
}

export async function POST(request: Request) {
    const res: ICreateUserRequest = await request.json();
    const { email,
        firstName,
        lastName,
        role } = res;
    const data = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        role: role
    }
    const user = await service.createUser(data)

    return NextResponse.json(user);
}

export async function PUT(request: Request) {
    const res: IUpdateUserRequest = await request.json();
    const { email,
        firstName,
        lastName,
        role,
        id } = res;
    const data = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        role: role
    }
    const updateUser = await service.updateUser(id, data);
    return NextResponse.json(updateUser);
}

export async function DELETE(request: Request) {
    const res: IDeleteUserRequest = await request.json();
    const { id } = res;
    const deleteUser = await service.deleteUser(id);
    return NextResponse.json(deleteUser);
}
