import { PrismaClient } from '@prisma/client';
import { NextApiRequest, NextApiResponse } from 'next';
import { NextResponse } from 'next/server';
import { UserService } from '@/lib/userService';

const service = UserService;

interface IUser {
    firstName: string,
    lastName: string,
    role: string,
    email: string
}

interface ICreateUserRequest {
    firstName: string,
    lastName: string,
    role: string,
    email: string
}

interface IUpdateUserRequest extends IUser {
    id: string
}

interface IDeleteUserRequest {
    id: string;
}

export async function GET(request: Request, {params}:{params: any}) {
    const {id} = params;
    const user = await service.getOneUser(id);
    return NextResponse.json(user);
}

export async function POST(request: Request) {
    const res: ICreateUserRequest = await request.json();
    const { email,
        firstName,
        lastName,
        role } = res;
    const data = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        role: role
    }
    const user = await service.createUser(data)

    return NextResponse.json(user);
}

export async function PUT(request: Request, {params}:{params: any}) {
    const res: IUpdateUserRequest = await request.json();
    const {id} = params;
    const { email,
        firstName,
        lastName,
        role} = res;
    const data = {
        email: email,
        firstName: firstName,
        lastName: lastName,
        role: role
    }
    const updateUser = await service.updateUser(id, data);
    return NextResponse.json(updateUser);
}

export async function DELETE(request: Request, {params}:{params: any}) {
    const {id} = params;
    const deleteUser = await service.deleteUser(id);
    return NextResponse.json(deleteUser);
}

