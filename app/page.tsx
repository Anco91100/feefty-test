import AddUserButton from '@/components/AddUserButton';
import UserList from '@/components/UserList';
import React from 'react'

export default function Home() {
  return (
    <div className='bg-[#F7F8FA] h-screen'>
      <div className='flex flex-col items-start w-screen h-64 gap-5'>
        <div className='flex flex-row justify-between items-center p-5 w-full'>
          <h1 className='items-center text-[#0E1823] font-medium text-xl'> Users</h1>
          <AddUserButton />
        </div>
        <UserList />
      </div>
    </div>
  )
}
