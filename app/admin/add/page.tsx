import FormAdd from '@/components/FormAdd';
import BackButton from "@/components/BackButton";
import React from 'react'

export default function AddUser() {
    return (
        <div className='bg-[#F7F8FA] h-screen'>
            <div className='flex flex-col items-start w-screen h-64 gap-5'>
                <div className='flex flex-col justify-center items-start p-5 w-full gap-3'>
                    <BackButton />
                    <h1 className='items-center text-[#0E1823] font-medium text-xl'> Add User</h1>
                </div>
                <FormAdd />
            </div>
        </div>
    )
}
