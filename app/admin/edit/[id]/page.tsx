import BackButton from "@/components/BackButton";
import FormEdit from "@/components/FormEdit";
import React from 'react'

type Props = {
    params: {
        id: number;
    };
    searchParams: {
        firstName: string;
        lastName: string;
    };
};
export default function EditUser(props: Props) {
    return (
        <div className='bg-[#F7F8FA] h-screen'>
            <div className='flex flex-col items-start w-screen h-64 gap-5'>
                <div className='flex flex-col justify-center items-start p-5 w-full gap-3'>
                    <BackButton />
                    <h1 className='items-center text-[#0E1823] font-medium text-xl'> Edit {props.searchParams.firstName} {props.searchParams.lastName}</h1>
                </div>
                <FormEdit id={props.params.id} />
            </div>
        </div>
    )
}