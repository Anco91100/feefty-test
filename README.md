**Feefty Admin Test**

Ce projet est un test technique qui consiste à développer une application web de gestion des utilisateurs en utilisant les technologies suivantes : **Next.js 13**, **TailwindCSS**, **Ant Design**, **SWR** et **Prisma** pour la manipulation de **PostgreSQL**. 
Le frontend utilise le pattern **Compound Component**, tandis que le backend adopte le pattern **Repository** pour le **CRUD** avec une **architecture MVC**, où le contrôleur gère les opérations CRUD, les vues sont construites en utilisant React et le modèle est défini avec Prisma.

Technologies utilisées

    Next.js 13 : Cette version de Next.js inclut le nouveau App Router, qui propose une nouvelle structure et une approche différente du routage en utilisant des Server Components et des Client Components. Les Server Components sont privilégiés, mais les Client Components sont utilisés pour les fonctionnalités interactives ou la gestion de l'état.

    TailwindCSS : Il s'agit d'un framework CSS utilitaire qui permet de créer rapidement des interfaces utilisateur en combinant des classes pré-définies. Il offre une grande flexibilité et facilite le développement d'interfaces cohérentes.

    Ant Design : C'est une bibliothèque de composants UI riche en fonctionnalités, prête à l'emploi et compatible avec React. Elle fournit des composants élégants et bien conçus qui peuvent être utilisés pour construire rapidement une interface utilisateur conviviale.

    SWR : Cette bibliothèque simplifie la gestion des requêtes et de la mise en cache des données en utilisant le concept de hooks. Elle facilite le fetch des données depuis un serveur et offre une gestion intégrée de la mise en cache, de la pagination et des revalidations.

    Prisma : Il s'agit d'un ORM (Object-Relational Mapping) moderne qui simplifie la manipulation des bases de données relationnelles. Prisma facilite la création et la gestion du schéma de la base de données et offre une interface conviviale pour effectuer des opérations CRUD.

    **Structure du projet**

Le projet est organisé selon une structure MVC (Modèle-Vue-Contrôleur) :

    Le contrôleur est responsable de la logique métier, notamment des opérations CRUD sur les utilisateurs. Il communique avec le modèle pour effectuer les requêtes à la base de données et retourner les résultats.

    Les vues sont construites en utilisant React et sont responsables de l'affichage des données et de la gestion des interactions utilisateur. Les composants sont conçus selon le pattern Compound Component, qui permet de composer des composants complexes en utilisant des composants plus petits et réutilisables.

    Le modèle est défini avec Prisma et représente la structure des données dans la base de données PostgreSQL. Prisma facilite la création et la gestion du schéma de la base de données et offre une interface intuitive pour effectuer des opérations de lecture, d'écriture et de mise à jour des données.

    **Tests**

Le projet comprend une suite de tests unitaires pour garantir la qualité du code. Ces tests peuvent être exécutés à l'aide de GitLab CI/CD, qui est intégré au projet. Lorsque vous poussez des modifications vers la branche principale, les tests unitaires sont exécutés automatiquement pour vérifier l'intégrité du code. Cela garantit que les fonctionnalités développées sont conformes aux spécifications et évite la régression du code existant.

Les tests unitaires sont écrits en utilisant des bibliothèques telles que Jest et React Testing Library. Jest est un framework de test populaire pour JavaScript, tandis que React Testing Library fournit des utilitaires pour effectuer des assertions et simuler des interactions utilisateur dans les tests React.



**Approche Repository Pattern**

Le projet utilise l'approche Repository Pattern pour gérer les opérations CRUD avec la base de données. Cette approche favorise la séparation des préoccupations en isolant la logique d'accès aux données dans des classes appelées "repositories".

Les repositories agissent comme une couche intermédiaire entre le contrôleur et le modèle. Ils encapsulent la logique spécifique à la récupération, la création, la mise à jour et la suppression des données. Cela permet de rendre le code du contrôleur plus propre et facilite la réutilisation de la logique d'accès aux données dans d'autres parties de l'application.

Dans le projet, les repositories sont responsables de l'exécution des requêtes Prisma pour interagir avec la base de données PostgreSQL. Ils fournissent des méthodes bien définies pour effectuer les opérations CRUD, ce qui rend le code du contrôleur plus expressif et déclaratif.

L'approche Repository Pattern présente les avantages suivants :

    Séparation claire des responsabilités : Les repositories se concentrent uniquement sur la manipulation des données, tandis que le contrôleur se concentre sur la logique métier.

    Réutilisation du code : Les méthodes de repository peuvent être réutilisées dans différents contrôleurs ou même dans d'autres projets.

    Testabilité améliorée : Les repositories peuvent être facilement testés de manière unitaire, ce qui facilite la vérification du bon fonctionnement des opérations de base de données.

Dans le projet, les repositories sont implémentés en utilisant Prisma pour exécuter les requêtes SQL. Chaque modèle de base de données a un repository associé qui expose des méthodes pour effectuer des opérations spécifiques sur ce modèle.

**Approche Compound Component Pattern**

Le projet utilise l'approche Compound Component Pattern pour structurer les composants React. Cette approche permet de créer des composants hautement réutilisables en permettant aux utilisateurs de personnaliser et de combiner différentes parties du composant selon leurs besoins.

Dans l'approche Compound Component Pattern, un composant parent expose des composants enfants (appelés composants composés) qui sont destinés à être utilisés ensemble. Les composants composés fonctionnent en étroite collaboration les uns avec les autres pour fournir une fonctionnalité cohérente et configurable.

Dans le projet, plusieurs composants utilisent le Compound Component Pattern pour offrir une flexibilité maximale à l'utilisateur. Par exemple, le composant User est un composant composé qui permet d'afficher les informations d'un utilisateur, ainsi que des fonctionnalités supplémentaires telles que la possibilité de basculer l'état de l'utilisateur ou de le modifier.

L'approche Compound Component Pattern présente les avantages suivants :

    Réutilisabilité : Les composants composés peuvent être utilisés dans différentes parties de l'application, offrant ainsi une solution modulaire et réutilisable.

    Configurabilité : Les utilisateurs peuvent configurer les composants composés en fournissant différents enfants avec des configurations spécifiques.

    Cohérence : Les composants composés permettent de maintenir une cohérence visuelle et fonctionnelle, car ils sont conçus pour fonctionner ensemble de manière harmonieuse.

**Virtualisation légère**

Je vous propose également de la virtualisation légère avec Docker-compose pour pouvoir démarrer rapidement l'application.
Les instructions pour démarrer les containers :
Installez docker-compose : https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-fr

Lancez les containers et montez les images
**sudo docker-compose up --build**

Ouvrez un autre terminal et lancer le service **db**:
**sudo docker-compose run db bash**
Une fois dans le container, éxecutez la commande suivante:
**psql --host=db --username=postgres --dbname=feefty**
Si cela fonctionne, la base de donné feefty est correctement monté

Ouvrez un autre terminal et lancer le service **app**:
**sudo docker-compose run app bash**
Une fois dans le container, éxecutez la commande suivante:
**npx prisma migrate deploy**

**Maintenant testez l'application dans le http://localhost:3000/**


Version node : 18
Version React : 18
Librarie utilisé : Formik, Ant design, heroicons, TailwindCSS
