import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient()

type User = {
    firstName: string,
    lastName: string,
    role: string,
    email: string
}

interface IUserService<T> {
    getAllUsers: () => Promise<T[]>;
    getOneUser:(id: string) => Promise<T | null>;
    createUser: (data: T) => Promise<T>;
    updateUser: (id: string, data: T) => Promise<T>;
    deleteUser: (id: string) => Promise<T>;
}
export const UserService: IUserService<User> = {
    getAllUsers: async () => await prisma.user.findMany(),
    getOneUser:async (id) => await prisma.user.findUnique({where: {id: Number(id)}}),
    createUser: async (data) => await prisma.user.create({data}),
    updateUser: async (id, data) => await prisma.user.update({where: {id: Number(id)}, data}),
    deleteUser: async (id) => await prisma.user.delete({where: {id: Number(id)}})
}