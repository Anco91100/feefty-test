'use client';
import { Formik, Field, ErrorMessage } from 'formik';
import axios from 'axios';
import toast, { Toaster } from 'react-hot-toast';
import { useRouter } from 'next/navigation';
import React from 'react'

interface Props {
    firstName: string;
    lastName: string;
    email: string;
    id: number;
    role: string;
};
export default function FormAdd() {
    const router = useRouter();
    const handleSubmit = async (values: any) => {
        try {
            await axios.post('/api/user', values);
            toast.success('Beautiful you added an user');
            router.push('/');
        } catch (error) {
            toast.error("Ooh no it didn't work")
            console.log(error);
        }
    };
    return (
        <div className='flex flex-col w-screen h-screen items-start gap-4' >
            <Toaster />
            <Formik initialValues={{ firstName: '', lastName: '', email: '', role: '' }} onSubmit={handleSubmit}>
                {({ handleSubmit }) => (
                    <form className='w-full h-full' onSubmit={handleSubmit}>
                        <div className='flex flex-col w-full h-[500px] items-start p-4 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]' >
                            <div className='flex flex-col min-w-full items-start h-16 gap-2'>
                                <h2> User info</h2>
                                <div className='border border-solid border-[rgba(188,202,220,0.5)] w-full'></div>
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">First Name</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="firstName" />
                                <ErrorMessage name="lastName" component="div" className="text-red-500 text-xs" />
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Last Name</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="text" name="lastName" />
                                <ErrorMessage name="lastName" component="div" className="text-red-500 text-xs" />
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Email address</h2>
                                <Field className="rounded-sm text-[#0E1823] border border-solid border-[#BCCADC] w-full h-9 p-3 gap-2 items-center" type="email" name="email" />
                                <ErrorMessage name="email" component="div" className="text-red-500 text-xs" />
                            </div>
                            <div className="flex flex-col min-w-full items-start h-24 gap-1">
                                <h2 className="text-[#627D98] font-normal text-xs">Role</h2>
                                <Field as="select" className="rounded-sm text-[#0E1823] bg-white border border-solid border-[#BCCADC] w-full h-11 p-3 gap-2 items-center" name="role">
                                    <option value="">Select a role</option>
                                    <option value="Account Manager">Account Manager</option>
                                    <option value="Developer">Developer</option>
                                    <option value="Grady best dev ❤️">Grady best dev ❤️ </option>
                                </Field>
                                <ErrorMessage name="role" component="div" className="text-red-500 text-xs" />
                            </div>
                        </div>
                        <div className='flex flex-row justify-end items-start p-4 h-7 w-full'>
                            <button type="submit" className='flex flex-row items-center justify-center p-4 gap-2 w-20 h-7 rounded-sm  border-solid border-[#104EE9] bg-[#104EE9] text-white text-center text-sm font-normal'> Save </button>
                        </div>
                    </form>
                )}
            </Formik>
        </div>
    )
}
