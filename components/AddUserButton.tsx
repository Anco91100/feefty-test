import Link from "next/link";
import React from 'react'

export default function AddUserButton() {
    return (
        <Link href="/admin/add" className='flex flex-row items-center text-center text-[#104EE9] font-normal text-sm gap-[6px]'>
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-[14px] h-[14px]">
                <path strokeLinecap="round" strokeLinejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
            </svg>
            Add
        </Link>
    )
}
