import { render, screen, waitFor } from '@testing-library/react';
import UserList from '../UserList';
import useSWR from 'swr';

jest.mock('swr', () => jest.fn());

describe('UserList component', () => {
  test('renders user list correctly', async () => {
    const mockData = [
      {
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@example.com',
        id: 1,
        role: 'User',
      },
      {
        firstName: 'Jane',
        lastName: 'Smith',
        email: 'jane.smith@example.com',
        id: 2,
        role: 'Admin',
      },
    ];

    (useSWR as jest.Mock).mockReturnValue({
      data: mockData,
      error: null,
      isLoading: false,
    });

    render(<UserList />);

    await waitFor(() => {
      expect(screen.getByText('John Doe')).toBeInTheDocument();
      expect(screen.getByText('john.doe@example.com')).toBeInTheDocument();
      expect(screen.getByText('Jane Smith')).toBeInTheDocument();
      expect(screen.getByText('jane.smith@example.com')).toBeInTheDocument();
    });
  });

  test('displays error message on data fetch failure', async () => {
    const errorMessage = 'Failed to fetch data';

    (useSWR as jest.Mock).mockReturnValue({
      data: null,
      error: errorMessage,
      isLoading: false,
    });

    render(<UserList />);

    expect(screen.getByText('échec du chargement')).toBeInTheDocument();
  });

  test('displays message when no users are present', async () => {
    const emptyData: [] = [];

    (useSWR as jest.Mock).mockReturnValue({
      data: emptyData,
      error: null,
      isLoading: false,
    });

    render(<UserList />);

    expect(screen.getByTestId('no-users-message')).toBeInTheDocument();
    expect(screen.getByText("Oh well...")).toBeInTheDocument();
  });
});
