import { render, screen } from '@testing-library/react';
import UserInfo from '../UserInfo';
import React from 'react'
import '@testing-library/jest-dom';

describe('UserInfo component', () => {
  test('renders user information correctly', () => {
    const user = {
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
    };

    render(<UserInfo {...user} />);

    expect(screen.getByText('John Doe')).toBeInTheDocument();
    expect(screen.getByText('john.doe@example.com')).toBeInTheDocument();
  });
});
