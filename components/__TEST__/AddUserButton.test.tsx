import { render, screen } from '@testing-library/react';
import React from 'react'
import '@testing-library/jest-dom';
import AddUserButton from '../AddUserButton';

test('renders AddUserButton component', () => {
  render(<AddUserButton />);
  
  const linkElement = screen.getByText('Add');
  expect(linkElement).toBeInTheDocument();
});
