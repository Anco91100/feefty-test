import { render, screen, fireEvent } from '@testing-library/react';
import User from '../User';
import React from 'react'
import '@testing-library/jest-dom';

describe('User component', () => {
  test('renders user information correctly', () => {
    const user = {
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      id: 1,
      role: 'user',
    };

    render(<User {...user} />);

    expect(screen.getByText('John Doe')).toBeInTheDocument();
    expect(screen.getByText('john.doe@example.com')).toBeInTheDocument();
  });

  test('toggles the switch', () => {
    const user = {
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      id: 1,
      role: 'user',
    };

    render(<User {...user} />);

    const switchElement = screen.getByRole('switch');

    expect(switchElement).toBeChecked();

    fireEvent.click(switchElement);

    expect(switchElement).not.toBeChecked();
  });
});
