import { render, screen } from '@testing-library/react';
import BackButton from '../BackButton';
import React from 'react'
import '@testing-library/jest-dom';


describe('BackButton', () => {
  test('renders the "Back" text', () => {
    render(<BackButton />);
    const backButton = screen.getByText('Back');
    expect(backButton).toBeInTheDocument();
  });
});
