'use client';
import useSWR from 'swr'
import User from '../components/User';
import { Skeleton } from 'antd';
import React from 'react'

const fetcher = (url: RequestInfo | URL) => fetch(url).then((res) => res.json());

export default function UserList() {
    const { data, error, isLoading }: { data: [{ firstName: string, lastName: string, email: string, id: number, role: string }], error: any, isLoading: boolean } = useSWR('/api/user', fetcher)
    if (error) return <div className='flex flex-row w-full h-full items-center justify-center' >échec du chargement</div>
    if (isLoading) return <div className='flex flex-col w-full h-full gap-4 items-center justify-center' >
        <Skeleton loading={isLoading} active />
        <Skeleton loading={isLoading} active />
    </div>
    return (
        <div className='flex flex-col w-full h-auto items-center gap-3 bg-white shadow-[0px_4px_20px_rgba(0,0,0,0.08)]' >
            {data.length <= 0 && <div data-testid="no-users-message">
                <p className="items-center text-center text-[#627D98] font-normal text-base">
                    Oh well...
                </p>
                <p className="items-center text-center text-[#627D98] font-normal text-base">
                    Looks like there are no users here yet.
                    <br />
                    Add a new user to see it here!
                </p>
            </div>}
            {data && data.map((person, _index) => {
                return (
                    <User key={person.id} {...person} />
                )
            })}
        </div>
    )
}
