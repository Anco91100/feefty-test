'use client';
import { useState } from 'react';
import { PencilSquareIcon } from '@heroicons/react/24/outline'
import { Switch } from 'antd';
import UserInfo from './UserInfo';
import Link from 'next/link';
import React from 'react'

interface Props {
    firstName: string;
    lastName: string;
    email: string;
    id: number;
    role: string;
};
export default function User(props: Props) {
    const [toggle, setToggle] = useState(true);
    const toggleSwitch = (checked: boolean) => {
        setToggle(checked);
      }
    return (
        <div className='flex flex-row justify-between items-center p-4 gap-3 h-20 w-full border-b border-solid border-[rgba(188,202,220,0.5)]'>
            <Switch checked={toggle} onChange={toggleSwitch} />
            <UserInfo firstName={props.firstName} email={props.email} lastName={props.lastName} />
            <Link href={'/admin/edit/'+props.id+'?firstName='+props.firstName+'&lastName='+props.lastName} shallow className='flex flex-row justify-center items-center p-2 gap-3 rounded-[100px] h-8 w-8 border border-solid border-[#104EE9] shadow-[0px_2px_0px_rgba(0,0,0,0.043)]'>
                <PencilSquareIcon  className='text-[#104EE9]' />
            </Link>
        </div>
    )
}
