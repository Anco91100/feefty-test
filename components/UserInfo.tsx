import React from 'react'

interface Props {
    firstName: string;
    lastName: string;
    email: string;
};
export default function UserInfo(props: Props) {
    return (
        <div className='flex flex-col items-start gap-1 w-60 h-10'>
            <p className='h-4 flex items-center text-[#0E1823] text-sm font-normal'> {props.firstName} {props.lastName} </p>
            <p className='h-4 flex items-center text-[#627D98] text-sm font-normal'> {props.email} </p>
        </div>
    )
}
